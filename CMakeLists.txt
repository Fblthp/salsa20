cmake_minimum_required(VERSION 3.5)
project(Salsa20)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp salsa.h)
add_executable(Salsa20 ${SOURCE_FILES})