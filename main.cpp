#include <cmath>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cmath>
#include <stack>
#include <functional>
#include <set>
#include <queue>
#include <string>
#include <map>
#include <sstream>
#include <iomanip>
#include <tiff.h>
#include <cassert>
#include "salsa.h"

using namespace std;

template<class T>
void AssertVecsEqual(const vector<T>& v1, const vector<T>& v2) {
    assert(v1.size() == v2.size());
    for (int i = 0; i < v1.size(); ++i) {
        assert(v1[i] == v2[i]);
    }
}

template<class T>
void AssertMatEqual(const vector<vector<T>>& mat1, const vector<vector<T>>& mat2) {
    assert(mat1.size() == mat2.size());
    for (int i = 0; i < mat1.size(); ++i) {
        AssertVecsEqual(mat1[i], mat2[i]);
    }
}

int main()
{
    auto s = Salsa();

    //test quarterround
    vector<uint32> t1 = {0x00000001, 0x00000000, 0x00000000, 0x00000000};
    s.quarterround(t1);
    vector<uint32> res1 = {0x08008145, 0x00000080, 0x00010200, 0x20500000};
    AssertVecsEqual(t1, res1);

    vector<uint32> t2 = {0xe7e8c006, 0xc4f9417d, 0x6479b4b2, 0x68c67137};
    s.quarterround(t2);
    vector<uint32> res2 = {0xe876d72b, 0x9361dfd5, 0xf1460244, 0x948541a3};
    AssertVecsEqual(t2, res2);



    //test rowround
    vector<vector<uint32>> tr1 = {
        {0x00000001, 0x00000000, 0x00000000, 0x00000000},
        {0x00000001, 0x00000000, 0x00000000, 0x00000000},
        {0x00000001, 0x00000000, 0x00000000, 0x00000000},
        {0x00000001, 0x00000000, 0x00000000, 0x00000000}
    };
    s.rowround(tr1);
    vector<vector<uint32>> resr1 = {
        {0x08008145, 0x00000080, 0x00010200, 0x20500000},
        {0x20100001, 0x00048044, 0x00000080, 0x00010000},
        {0x00000001, 0x00002000, 0x80040000, 0x00000000},
        {0x00000001, 0x00000200, 0x00402000, 0x88000100}
    };
    AssertMatEqual(tr1, resr1);


    vector<vector<uint32>> tr2 = {
        {0x08521bd6, 0x1fe88837, 0xbb2aa576, 0x3aa26365},
        {0xc54c6a5b, 0x2fc74c2f, 0x6dd39cc3, 0xda0a64f6},
        {0x90a2f23d, 0x067f95a6, 0x06b35f61, 0x41e4732e},
        {0xe859c100, 0xea4d84b7, 0x0f619bff, 0xbc6e965a}
    };
    s.rowround(tr2);
    vector<vector<uint32>> resr2 = {
        {0xa890d39d, 0x65d71596, 0xe9487daa, 0xc8ca6a86},
        {0x949d2192, 0x764b7754, 0xe408d9b9, 0x7a41b4d1},
        {0x3402e183, 0x3c3af432, 0x50669f96, 0xd89ef0a8},
        {0x0040ede5, 0xb545fbce, 0xd257ed4f, 0x1818882d}
    };
    AssertMatEqual(tr2, resr2);



    //test columnround
    vector<vector<uint32>> tc1 = {
        {0x00000001, 0x00000000, 0x00000000, 0x00000000},
        {0x00000001, 0x00000000, 0x00000000, 0x00000000},
        {0x00000001, 0x00000000, 0x00000000, 0x00000000},
        {0x00000001, 0x00000000, 0x00000000, 0x00000000}
    };
    s.columnround(tc1);
    vector<vector<uint32>> resc1 = {
        {0x10090288, 0x00000000, 0x00000000, 0x00000000},
        {0x00000101, 0x00000000, 0x00000000, 0x00000000},
        {0x00020401, 0x00000000, 0x00000000, 0x00000000},
        {0x40a04001, 0x00000000, 0x00000000, 0x00000000}
    };
    AssertMatEqual(tc1, resc1);


    vector<vector<uint32>> tc2 = {
        {0x08521bd6, 0x1fe88837, 0xbb2aa576, 0x3aa26365},
        {0xc54c6a5b, 0x2fc74c2f, 0x6dd39cc3, 0xda0a64f6},
        {0x90a2f23d, 0x067f95a6, 0x06b35f61, 0x41e4732e},
        {0xe859c100, 0xea4d84b7, 0x0f619bff, 0xbc6e965a}
    };
    s.columnround(tc2);
    vector<vector<uint32>> resc2 = {
        {0x8c9d190a, 0xce8e4c90, 0x1ef8e9d3, 0x1326a71a},
        {0x90a20123, 0xead3c4f3, 0x63a091a0, 0xf0708d69},
        {0x789b010c, 0xd195a681, 0xeb7d5504, 0xa774135c},
        {0x481c2027, 0x53a8e4b5, 0x4c1f89c5, 0x3f78c9c8}
    };
    AssertMatEqual(tc2, resc2);



    //test doubleround
    vector<vector<uint32>> td1 = {
        {0x00000001, 0x00000000, 0x00000000, 0x00000000},
        {0x00000000, 0x00000000, 0x00000000, 0x00000000},
        {0x00000000, 0x00000000, 0x00000000, 0x00000000},
        {0x00000000, 0x00000000, 0x00000000, 0x00000000}
    };
    s.doubleround(td1);
    vector<vector<uint32>> resd1 = {
        {0x8186a22d, 0x0040a284, 0x82479210, 0x06929051},
        {0x08000090, 0x02402200, 0x00004000, 0x00800000},
        {0x00010200, 0x20400000, 0x08008104, 0x00000000},
        {0x20500000, 0xa0000040, 0x0008180a, 0x612a8020}
    };
    AssertMatEqual(td1, resd1);


    vector<vector<uint32>> td2 = {
        {0xde501066, 0x6f9eb8f7, 0xe4fbbd9b, 0x454e3f57},
        {0xb75540d3, 0x43e93a4c, 0x3a6f2aa0, 0x726d6b36},
        {0x9243f484, 0x9145d1e8, 0x4fa9d247, 0xdc8dee11},
        {0x054bf545, 0x254dd653, 0xd9421b6d, 0x67b276c1}
    };
    s.doubleround(td2);
    vector<vector<uint32>> resd2 = {
        {0xccaaf672, 0x23d960f7, 0x9153e63a, 0xcd9a60d0},
        {0x50440492, 0xf07cad19, 0xae344aa0, 0xdf4cfdfc},
        {0xca531c29, 0x8e7943db, 0xac1680cd, 0xd503ca00},
        {0xa74b2ad6, 0xbc331c5c, 0x1dda24c7, 0xee928277}
    };
    AssertMatEqual(td2, resd2);



    //test littleendian
    vector<unsigned char> b1 = {86, 75, 30, 9};
    uint32 le1 = 0x091e4b56;
    assert(s.littleendian(b1) == le1);
    auto le_inv1 = s.littleendian_inv(le1);
    AssertVecsEqual(le_inv1, b1);


    vector<unsigned char> b2 = {255, 255, 255, 250};
    uint32 le2 = 0xfaffffff;
    assert(s.littleendian(b2) == le2);
    auto le_inv2 = s.littleendian_inv(le2);
    AssertVecsEqual(le_inv2, b2);



    //test salsa20
    vector<unsigned char> tsalsa1(64, 0);
    auto res1_salsa = s.salsa20(tsalsa1);
    AssertVecsEqual(tsalsa1, res1_salsa);


    vector<unsigned char> tsalsa2 = {
            211,159, 13,115, 76, 55, 82,183, 3,117,222, 37,191,187,234,136,
            49,237,179, 48, 1,106,178,219,175,199,166, 48, 86, 16,179,207,
            31,240, 32, 63, 15, 83, 93,161,116,147, 48,113,238, 55,204, 36,
            79,201,235, 79, 3, 81,156, 47,203, 26,244,243, 88,118,104, 54
    };
    auto res2_salsa = s.salsa20(tsalsa2);
    vector<unsigned char> exp_res2_salsa = {
            109, 42,178,168,156,240,248,238,168,196,190,203, 26,110,170,154,
            29, 29,150, 26,150, 30,235,249,190,163,251, 48, 69,144, 51, 57,
            118, 40,152,157,180, 57, 27, 94,107, 42,236, 35, 27,111,114,114,
            219,236,232,135,111,155,110, 18, 24,232, 95,158,179, 19, 48,202
    };
    AssertVecsEqual(res2_salsa, exp_res2_salsa);



    //test salsa20 encrypt
    vector<unsigned char> key(32);
    vector<unsigned char> n(16);

    for (unsigned char i = 0; i < 16; ++i) {
        key[i] = i + 1;
        key[i + 16] = i + 201;
        n[i] = i + 101;
    }

    vector<unsigned char> seq = {
        101, 120, 112, 97,
        110, 100, 32, 51,
        50, 45, 98, 121,
        116, 101, 32, 107
    };

    vector<unsigned char> res = s.salsa20_expansion(key, n, seq);
    vector<unsigned char> exp_res = {
        69, 37, 68, 39, 41, 15,107,193,255,139,122, 6,170,233,217, 98,
        89,144,182,106, 21, 51,200, 65,239, 49,222, 34,215,114, 40,126,
        104,197, 7,225,197,153, 31, 2,102, 78, 76,176, 84,245,246,184,
        177,160,133,130, 6, 72,149,119,192,195,132,236,234,103,246, 74
    };
    AssertVecsEqual(res, exp_res);


    vector<unsigned char> message(1000);
    for (int i = 0; i < message.size(); ++i) {
        message[i] = rand() % 256;
    }
    vector<unsigned char> res_msg = message;

    //encrypt
    for (int i = 0; i < message.size(); i += 64) {
        s.crypt(key, n, seq, message.begin() + i, min(message.begin() + i + 64, message.end()));
    }

    //decrypt
    for (int i = 0; i < message.size(); i += 64) {
        s.crypt(key, n, seq, message.begin() + i, min(message.begin() + i + 64, message.end()));
    }
    AssertVecsEqual(message, res_msg);
}
