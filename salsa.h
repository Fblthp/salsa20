#pragma once
#include <cmath>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cmath>
#include <stack>
#include <functional>
#include <set>
#include <queue>
#include <string>
#include <map>
#include <sstream>
#include <iomanip>
#include <tiff.h>

using namespace std;

class Salsa {
public:
    uint32 rotate(uint32 x, int sh) {
        return (((x) << (sh)) | ((x) >> (32 - (sh))));
    }

    void quarterround(vector<uint32>& xs) {
        xs[1] = xs[1] ^ (rotate((xs[0] + xs[3]), 7));
        xs[2] = xs[2] ^ (rotate((xs[1] + xs[0]), 9));
        xs[3] = xs[3] ^ (rotate((xs[2] + xs[1]), 13));
        xs[0] = xs[0] ^ (rotate((xs[3] + xs[2]), 18));
    }

    void shiftVector(vector<uint32>& vec, int sh) {
        vector<uint32> res(4);
        for (int i = 0; i < 4; ++i) {
            res[i] = vec[(i + sh) % 4];
        }
        vec = move(res);
    }

    void rowround(vector<vector<uint32>>& mat) {
        for (int i = 0; i < 4; ++i) {
            shiftVector(mat[i], i);
            quarterround(mat[i]);
            shiftVector(mat[i], 4 - i);
        }
    }

    void transpose(vector<vector<uint32>>& mat) {
        for (int i = 0; i < 4; ++i) {
            for (int j = i + 1; j < 4; ++j) {
                swap(mat[i][j], mat[j][i]);
            }
        }
    }

    void columnround(vector<vector<uint32>>& mat) {
        transpose(mat);
        rowround(mat);
        transpose(mat);
    }

    void doubleround(vector<vector<uint32>>& mat) {
        columnround(mat);
        rowround(mat);
    }

    uint32 littleendian(const vector<unsigned char>& b) {
        return b[0] + (1 << 8) * b[1] + (1 << 16) * b[2] + (1 << 24) * b[3];
    }

    vector<unsigned char> littleendian_inv(uint32 x) {
        vector<unsigned char> result(4);
        for (int i = 0; i < 4; ++i) {
            result[i] = x % (1 << 8);
            x /= (1 << 8);
        }
        return result;
    }

    vector<unsigned char> mat_to_char_seq(const vector<vector<uint32>>& mat) {
        vector<unsigned char> result;
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                auto le = littleendian_inv(mat[i][j]);
                for (auto sym: le) {
                    result.push_back(sym);
                }
            }
        }
        return result;
    }

    void salsa20(vector<vector<uint32>>& mat) {
        vector<vector<uint32>> salsaMat = mat;
        for (int i = 0; i < 10; ++i) {
            doubleround(salsaMat);
        }
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                mat[i][j] += salsaMat[i][j];
            }
        }
    }

    vector<unsigned char> salsa20(const vector<unsigned char>& xs) {
        assert(xs.size() == 64);
        vector<vector<uint32>> mat(4, vector<uint32>(4));
        for (int i = 0; i < 4; ++i) {
            for (int j = 0; j < 4; ++j) {
                mat[i][j] = littleendian({xs[16 * i + 4 * j], xs[16 * i + 4 * j + 1],
                                          xs[16 * i + 4 * j + 2], xs[16 * i + 4 * j + 3]});
            }
        }

        salsa20(mat);
        return mat_to_char_seq(mat);
    }

    vector<uint32> vector_to_littleendian(const vector<unsigned char>& source) {
        assert(source.size() % 4 == 0);
        vector<uint32> result(source.size() / 4);
        for (int i = 0; i < result.size(); ++i) {
            result[i] = littleendian({source[4 * i], source[4 * i + 1], source[4 * i + 2], source[4 * i + 3]});
        }
        return result;
    }

    vector<unsigned char> salsa20_expansion(const vector<unsigned char>& key,
                                        const vector<unsigned char>& nonce,
                                        const vector<unsigned char>& seq) {
        assert(key.size() == 32);
        vector<uint32> key_ = vector_to_littleendian(key);

        assert(nonce.size() == 16);
        vector<uint32> nonce_ = vector_to_littleendian(nonce);

        assert(seq.size() == 16);
        vector<uint32> seq_ = vector_to_littleendian(seq);

        vector<vector<uint32>> mat = {
            {seq_[0], key_[0], key_[1], key_[2]},
            {key_[3], seq_[1], nonce_[0], nonce_[1]},
            {nonce_[2], nonce_[3], seq_[2], key_[4]},
            {key_[5], key_[6], key_[7], seq_[3]}
        };

        salsa20(mat);
        return mat_to_char_seq(mat);
    }

    void crypt(const vector<unsigned char>& key,
                                const vector<unsigned char>& nonce,
                                const vector<unsigned char>& seq,
                                vector<unsigned char>::iterator msg_begin,
                                vector<unsigned char>::iterator msg_end) {

        assert(msg_end - msg_begin <= 64);
        vector<unsigned char> exp = salsa20_expansion(key, nonce, seq);
        for (auto it = msg_begin; it != msg_end; ++it) {
            *it = *it ^ exp[it - msg_begin];
        }
    }
};